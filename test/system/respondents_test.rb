require "application_system_test_case"

class RespondentsTest < ApplicationSystemTestCase
  setup do
    @respondent = respondents(:one)
  end

  test "visiting the index" do
    visit respondents_url
    assert_selector "h1", text: "Respondents"
  end

  test "creating a Respondent" do
    visit respondents_url
    click_on "New Respondent"

    fill_in "Name", with: @respondent.name
    fill_in "Survey", with: @respondent.survey_id
    click_on "Create Respondent"

    assert_text "Respondent was successfully created"
    click_on "Back"
  end

  test "updating a Respondent" do
    visit respondents_url
    click_on "Edit", match: :first

    fill_in "Name", with: @respondent.name
    fill_in "Survey", with: @respondent.survey_id
    click_on "Update Respondent"

    assert_text "Respondent was successfully updated"
    click_on "Back"
  end

  test "destroying a Respondent" do
    visit respondents_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Respondent was successfully destroyed"
  end
end
