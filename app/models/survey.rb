class Survey < ApplicationRecord
    
    has_many :questions
    has_many :respondents

    accepts_nested_attributes_for :questions

end
