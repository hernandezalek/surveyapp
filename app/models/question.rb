class Question < ApplicationRecord
  
  belongs_to :survey
  
  has_many :question_options
  has_many :answers

  accepts_nested_attributes_for :question_options

end
