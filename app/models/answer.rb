class Answer < ApplicationRecord
  
  belongs_to :question
  belongs_to :respondent
  
  has_many :answer_options
  accepts_nested_attributes_for :answer_options

end
