class RespondentsController < ApplicationController
  before_action :set_respondent, only: [:show, :edit, :update, :destroy]
  before_action :set_survey, only: [:new, :edit, :create]

  # GET /respondents
  # GET /respondents.json
  def index
    @respondents = Respondent.all
  end

  # GET /respondents/1
  # GET /respondents/1.json
  def show
  end

  # GET /respondents/new
  def new
    @survey = Survey.find(params[:survey_id])
    @respondent = Respondent.new
    @answer = @respondent.answers.build
    @answer_option = @answer.answer_options.build
  end

  # GET /respondents/1/edit
  def edit
  end

  # POST /respondents
  # POST /respondents.json
  def create
    @respondent = Respondent.new(respondent_params)
    @respondent.survey = @survey

    respond_to do |format|
      if @respondent.save
        format.html { redirect_to survey_respondent_path(@respondent.survey, @respondent), notice: 'Respondent was successfully created.' }
        format.json { render :show, status: :created, location: @respondent }
      else
        format.html { render :new }
        format.json { render json: @respondent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /respondents/1
  # PATCH/PUT /respondents/1.json
  def update
    respond_to do |format|
      if @respondent.update(respondent_params)
        format.html { redirect_to @respondent, notice: 'Respondent was successfully updated.' }
        format.json { render :show, status: :ok, location: @respondent }
      else
        format.html { render :edit }
        format.json { render json: @respondent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /respondents/1
  # DELETE /respondents/1.json
  def destroy
    @respondent.destroy
    respond_to do |format|
      format.html { redirect_to respondents_url, notice: 'Respondent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_respondent
      @respondent = Respondent.find(params[:id])
    end

    def set_survey
      @survey = Survey.find(params[:survey_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def respondent_params
      params.require(:respondent).permit(:name, :survey_id, answers_attributes: [ :id, :question_id, :respondent_id, answer_options_attributes: [ :id, :answer_id, :question_option_id ] ])
    end
end
