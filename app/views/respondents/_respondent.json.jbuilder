json.extract! respondent, :id, :name, :survey_id, :created_at, :updated_at
json.url respondent_url(respondent, format: :json)
