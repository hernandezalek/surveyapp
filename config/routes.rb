Rails.application.routes.draw do

  resources :surveys do 
    resources :respondents
  end

end
